-------------sql advanced--------------------
-----------------td3-------------------------
---------elnaz sherafat----------------------

---Creat tables------------------------------


---------------Person------------------

drop table if exists person;
CREATE TABLE person
(
	person_id 				int PRIMARY KEY 	NOT NULL,
	person_name 			varchar(100) 		NOT NULL,
	health_status 			varchar(100) 		NOT NULL,
	confirmed_status_date 	timestamp 			NOT NULL
);


----------------Place-----------------

drop table if exists place;
CREATE TABLE place
(
	place_id 	int PRIMARY KEY 	NOT NULL,
	place_name	varchar(100) 		NOT NULL,
	place_type 	varchar(100)		NOT NULL 
);

--------------Visit---------------------
 
drop table if exists visit;
CREATE TABLE visit
(
	visit_id 		int PRIMARY KEY 	NOT NULL,
	start_datetime 	timestamp 			NOT NULL,
	end_datetime 	timestamp 			NOT NULL,
	person_id 		int 				NOT NULL,
	place_id 		int 				NOT NULL,

	FOREIGN KEY ("person_id")	REFERENCES "person"("person_id"),
	FOREIGN KEY ("place_id") 	REFERENCES "place"("place_id")
);

-------------------------------------------------------------------
--Q1-Noms + statuts santé des personnes que Landyn Greer a croisé (même lieu, même moment)
-------------------------------------------------------------------
---Create a table(t1) with all information about Landyn Greer
drop table if exists t1 
create table t1 as 
select p.person_name, v.place_id, v.start_datetime, v.end_datetime, health_status
from visit v , person p 
where 	person_name = 'Landyn Greer' and v.person_id = p.person_id ;

select * from t1
--------------------------------------------------------------------
select  *
from t1 t inner join  visit v
	where  	  t.place_id = v.place_id 
		and   ((v.start_datetime >= t.start_datetime 
		and   v.start_datetime <= t.end_datetime)
        or	  (v.end_datetime >= t.start_datetime
		and   v.end_datetime <= t.end_datetime));
--------------------------------------------------------------------
select p.person_id, p.person_name, p.health_status 
from person p ,
	(select v.person_id   
	from t1 as t, visit as v 
	where t.place_id = v.place_id 
	and ((v.start_datetime >= t.start_datetime and v.start_datetime <= t.end_datetime)
	or (v.end_datetime >= t.start_datetime
	and v.end_datetime <= t.end_datetime))) p1
where p.person_id = p1.person_id and p.person_id <> 1 
;
-----------------------------------------------------------
-----------------------------------------------------------------
--Q2--Nombre de malades croisés dans un bar (même moment)---------

select count(distinct p1.person_id)
from 	person as p1, person as P2, visit as v1, 
		visit as v2, place as pl1, place as pl2 
where p1.health_status = 'Sick'
and pl1.place_type = 'Bar'
and p1.person_id != p2.person_id
and v1.person_id =  p1.person_id 
and v2.person_id =  p2.person_id
and v1.place_id  =  v2.place_id 
and pl2.place_id =  v2.place_id 
and pl1.place_id =  v1.place_id 
and ((v1.start_datetime >= v2.start_datetime 
and v1.start_datetime <= v2.end_datetime)
or 	(v1.end_datetime >= v2.start_datetime
and  v1.end_datetime <= v2.end_datetime));

select * 
from person p , visit v ,place p1
where p.health_status = 'Sick'
and p1.place_type = 'Bar'
and p.person_id = v.person_id
and v.place_id = p1.place_id
and p.confirmed_status_date <= v.end_datetime ;

-------------------------------------------------------------------
--Q3-Noms des personnes que Taylor Luna a croisé

---Create a table(t1) with all information about Taylor Luna
drop table if exists t2 
create table t2 as 
select 	p.person_name, 		p.person_id ,	v.place_id, 
		v.start_datetime, 	v.end_datetime, health_status
from 	visit v, 			person p 
where 	person_name = 'Taylor Luna' and v.person_id = p.person_id ;

select *from t2
-----------------------------------------------------------
select 	p.person_id, 	p.person_name, 	p.health_status 
from 	person p ,
		(select 	v.person_id   
		from t2 as t, visit as v 
		where t.place_id = v.place_id 
		and 	((v.start_datetime >= t.start_datetime and 
				v.start_datetime <= t.end_datetime)
		or 		(v.end_datetime >= t.start_datetime
		and 	v.end_datetime <= t.end_datetime))) p1
where 	p.person_id = p1.person_id and 	p.person_id <> 4 ;
-----------------------------------------------------------------
-------------------------------------------------------------------
--Q4-Nombre de malades par endroit (place_name) + durée moyenne de leurs visites dans cet endroi
select count(p.person_id) as nbr_malades, p1.place_name,
avg(v.end_datetime - v.start_datetime) as duree_moyenne 
from person as p, visit as v, place p1
where p.health_status = 'Sick'
and v.person_id = p.person_id 
and p1.place_id = v.place_id 
group by p1.place_name ;
-------------------------------------------------------------------
--Q5- Nombre de sains (non malades) par endroit (place_name) + durée moyenne de leurs visites dans cet endroit
select count(p.person_id) as nbr_sains, p1.place_name,
avg(v.end_datetime - v.start_datetime) as duree_moyenne 
from person as p, visit as v, place p1
where p.health_status = 'Healthy'
and v.person_id = p.person_id 
and p1.place_id = v.place_id 
group by p1.place_name ;
---------------------------------------------------------------------




